#!/bin/sh

BRICK_PATH="./gen/bricks/"
BRICK_NAME="ui_component_test"

cd ../../ &&
ls &&
mason new $BRICK_NAME -o $BRICK_PATH


# Add to mason.yaml
#   <BRICK_NAME>:
#    path: ./gen/bricks/<BRICK_NAME>/
# Console run script:
# >: mason get
