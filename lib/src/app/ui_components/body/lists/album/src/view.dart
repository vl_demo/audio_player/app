part of '../index.dart';

class APAlbumListView extends StatelessWidget {
  APAlbumListView({
    required APAlbumListModel model,
    required this.onSelect,
    super.key,
  }) : controller = APAlbumListController(
          model: model,
        );

  const APAlbumListView.controller({
    required this.controller,
    required this.onSelect,
    super.key,
  });

  final APAlbumListController controller;
  final void Function(Audio audio) onSelect;

  @override
  Widget build(BuildContext context) {
    return ValueListenableBuilder<APAlbumListModel>(
      valueListenable: controller,
      builder: (
        BuildContext context,
        APAlbumListModel value,
        Widget? child,
      ) {
        return ListView.builder(
          itemCount: value.list.length,
          itemBuilder: (
            BuildContext context,
            int index,
          ) {
            return GestureDetector(
              onTap: () {
                controller.selectIndex = index;
                onSelect(value.list[index]);
              },
              child: APSongItem(
                model: APSongItemModel(
                  title: value.list[index].metas.title ?? '',
                  subtitle: '${value.list[index].metas.artist} "${value.list[index].metas.album}"',
                  selected: value.selectedIndex == index,
                  imagePath: value.list[index].metas.image?.path,
                ),
              ),
            );
          },
        );
      },
    );
  }
}
