import 'package:assets_audio_player/assets_audio_player.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'model.freezed.dart';

@freezed
class APAlbumListModel with _$APAlbumListModel {
  const factory APAlbumListModel({
    required int selectedIndex,
    required List<Audio> list,
  }) = _APAlbumListModel;
}
