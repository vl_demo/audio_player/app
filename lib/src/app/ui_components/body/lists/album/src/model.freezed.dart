// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'model.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

/// @nodoc
mixin _$APAlbumListModel {
  int get selectedIndex => throw _privateConstructorUsedError;
  List<Audio> get list => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $APAlbumListModelCopyWith<APAlbumListModel> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $APAlbumListModelCopyWith<$Res> {
  factory $APAlbumListModelCopyWith(
          APAlbumListModel value, $Res Function(APAlbumListModel) then) =
      _$APAlbumListModelCopyWithImpl<$Res, APAlbumListModel>;
  @useResult
  $Res call({int selectedIndex, List<Audio> list});
}

/// @nodoc
class _$APAlbumListModelCopyWithImpl<$Res, $Val extends APAlbumListModel>
    implements $APAlbumListModelCopyWith<$Res> {
  _$APAlbumListModelCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? selectedIndex = null,
    Object? list = null,
  }) {
    return _then(_value.copyWith(
      selectedIndex: null == selectedIndex
          ? _value.selectedIndex
          : selectedIndex // ignore: cast_nullable_to_non_nullable
              as int,
      list: null == list
          ? _value.list
          : list // ignore: cast_nullable_to_non_nullable
              as List<Audio>,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$APAlbumListModelImplCopyWith<$Res>
    implements $APAlbumListModelCopyWith<$Res> {
  factory _$$APAlbumListModelImplCopyWith(_$APAlbumListModelImpl value,
          $Res Function(_$APAlbumListModelImpl) then) =
      __$$APAlbumListModelImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({int selectedIndex, List<Audio> list});
}

/// @nodoc
class __$$APAlbumListModelImplCopyWithImpl<$Res>
    extends _$APAlbumListModelCopyWithImpl<$Res, _$APAlbumListModelImpl>
    implements _$$APAlbumListModelImplCopyWith<$Res> {
  __$$APAlbumListModelImplCopyWithImpl(_$APAlbumListModelImpl _value,
      $Res Function(_$APAlbumListModelImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? selectedIndex = null,
    Object? list = null,
  }) {
    return _then(_$APAlbumListModelImpl(
      selectedIndex: null == selectedIndex
          ? _value.selectedIndex
          : selectedIndex // ignore: cast_nullable_to_non_nullable
              as int,
      list: null == list
          ? _value._list
          : list // ignore: cast_nullable_to_non_nullable
              as List<Audio>,
    ));
  }
}

/// @nodoc

class _$APAlbumListModelImpl implements _APAlbumListModel {
  const _$APAlbumListModelImpl(
      {required this.selectedIndex, required final List<Audio> list})
      : _list = list;

  @override
  final int selectedIndex;
  final List<Audio> _list;
  @override
  List<Audio> get list {
    if (_list is EqualUnmodifiableListView) return _list;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_list);
  }

  @override
  String toString() {
    return 'APAlbumListModel(selectedIndex: $selectedIndex, list: $list)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$APAlbumListModelImpl &&
            (identical(other.selectedIndex, selectedIndex) ||
                other.selectedIndex == selectedIndex) &&
            const DeepCollectionEquality().equals(other._list, _list));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType, selectedIndex, const DeepCollectionEquality().hash(_list));

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$APAlbumListModelImplCopyWith<_$APAlbumListModelImpl> get copyWith =>
      __$$APAlbumListModelImplCopyWithImpl<_$APAlbumListModelImpl>(
          this, _$identity);
}

abstract class _APAlbumListModel implements APAlbumListModel {
  const factory _APAlbumListModel(
      {required final int selectedIndex,
      required final List<Audio> list}) = _$APAlbumListModelImpl;

  @override
  int get selectedIndex;
  @override
  List<Audio> get list;
  @override
  @JsonKey(ignore: true)
  _$$APAlbumListModelImplCopyWith<_$APAlbumListModelImpl> get copyWith =>
      throw _privateConstructorUsedError;
}
