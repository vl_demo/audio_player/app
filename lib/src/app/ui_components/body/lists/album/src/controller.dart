part of '../index.dart';

class APAlbumListController extends ValueNotifier<APAlbumListModel> {
  APAlbumListController({
    required APAlbumListModel model,
  }) : super(
          model,
        );

  set selectIndex(int v) {
    value = value.copyWith(selectedIndex: v);
  }
}
