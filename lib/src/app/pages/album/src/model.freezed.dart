// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'model.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#adding-getters-and-methods-to-our-models');

/// @nodoc
mixin _$APAlbumPageStateModel {
  String? get title => throw _privateConstructorUsedError;
  List<Audio> get audios => throw _privateConstructorUsedError;

  @JsonKey(ignore: true)
  $APAlbumPageStateModelCopyWith<APAlbumPageStateModel> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $APAlbumPageStateModelCopyWith<$Res> {
  factory $APAlbumPageStateModelCopyWith(APAlbumPageStateModel value,
          $Res Function(APAlbumPageStateModel) then) =
      _$APAlbumPageStateModelCopyWithImpl<$Res, APAlbumPageStateModel>;
  @useResult
  $Res call({String? title, List<Audio> audios});
}

/// @nodoc
class _$APAlbumPageStateModelCopyWithImpl<$Res,
        $Val extends APAlbumPageStateModel>
    implements $APAlbumPageStateModelCopyWith<$Res> {
  _$APAlbumPageStateModelCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? title = freezed,
    Object? audios = null,
  }) {
    return _then(_value.copyWith(
      title: freezed == title
          ? _value.title
          : title // ignore: cast_nullable_to_non_nullable
              as String?,
      audios: null == audios
          ? _value.audios
          : audios // ignore: cast_nullable_to_non_nullable
              as List<Audio>,
    ) as $Val);
  }
}

/// @nodoc
abstract class _$$APAlbumPageStateModelImplCopyWith<$Res>
    implements $APAlbumPageStateModelCopyWith<$Res> {
  factory _$$APAlbumPageStateModelImplCopyWith(
          _$APAlbumPageStateModelImpl value,
          $Res Function(_$APAlbumPageStateModelImpl) then) =
      __$$APAlbumPageStateModelImplCopyWithImpl<$Res>;
  @override
  @useResult
  $Res call({String? title, List<Audio> audios});
}

/// @nodoc
class __$$APAlbumPageStateModelImplCopyWithImpl<$Res>
    extends _$APAlbumPageStateModelCopyWithImpl<$Res,
        _$APAlbumPageStateModelImpl>
    implements _$$APAlbumPageStateModelImplCopyWith<$Res> {
  __$$APAlbumPageStateModelImplCopyWithImpl(_$APAlbumPageStateModelImpl _value,
      $Res Function(_$APAlbumPageStateModelImpl) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? title = freezed,
    Object? audios = null,
  }) {
    return _then(_$APAlbumPageStateModelImpl(
      title: freezed == title
          ? _value.title
          : title // ignore: cast_nullable_to_non_nullable
              as String?,
      audios: null == audios
          ? _value._audios
          : audios // ignore: cast_nullable_to_non_nullable
              as List<Audio>,
    ));
  }
}

/// @nodoc

class _$APAlbumPageStateModelImpl implements _APAlbumPageStateModel {
  const _$APAlbumPageStateModelImpl(
      {this.title, required final List<Audio> audios})
      : _audios = audios;

  @override
  final String? title;
  final List<Audio> _audios;
  @override
  List<Audio> get audios {
    if (_audios is EqualUnmodifiableListView) return _audios;
    // ignore: implicit_dynamic_type
    return EqualUnmodifiableListView(_audios);
  }

  @override
  String toString() {
    return 'APAlbumPageStateModel(title: $title, audios: $audios)';
  }

  @override
  bool operator ==(Object other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$APAlbumPageStateModelImpl &&
            (identical(other.title, title) || other.title == title) &&
            const DeepCollectionEquality().equals(other._audios, _audios));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType, title, const DeepCollectionEquality().hash(_audios));

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$APAlbumPageStateModelImplCopyWith<_$APAlbumPageStateModelImpl>
      get copyWith => __$$APAlbumPageStateModelImplCopyWithImpl<
          _$APAlbumPageStateModelImpl>(this, _$identity);
}

abstract class _APAlbumPageStateModel implements APAlbumPageStateModel {
  const factory _APAlbumPageStateModel(
      {final String? title,
      required final List<Audio> audios}) = _$APAlbumPageStateModelImpl;

  @override
  String? get title;
  @override
  List<Audio> get audios;
  @override
  @JsonKey(ignore: true)
  _$$APAlbumPageStateModelImplCopyWith<_$APAlbumPageStateModelImpl>
      get copyWith => throw _privateConstructorUsedError;
}
