import 'package:assets_audio_player/assets_audio_player.dart';
import 'package:freezed_annotation/freezed_annotation.dart';

part 'model.freezed.dart';

@freezed
class APAlbumPageStateModel with _$APAlbumPageStateModel {
  const factory APAlbumPageStateModel({
    String? title,
    required List<Audio> audios,
  }) = _APAlbumPageStateModel;
}
