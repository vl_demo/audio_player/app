part of '../index.dart';

class APAlbumPageArgs {
  APAlbumPageArgs({
    required this.title,
  });

  final String title;
}

class APAlbumPageView extends StatefulWidget {
  const APAlbumPageView({
    required this.args,
    super.key,
  }) : super();

  final APAlbumPageArgs args;

  @override
  State<APAlbumPageView> createState() => _APAlbumPageViewState();
}

class _APAlbumPageViewState extends State<APAlbumPageView> {
  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    context.read<APAlbumPageBlocController>().close();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (_) => APAlbumPageBlocController(args: widget.args),
      child: BlocBuilder<APAlbumPageBlocController, APAlbumPageStateModel>(builder: (
        context,
        state,
      ) {
        final APAlbumPageBlocController controller = context.read<APAlbumPageBlocController>();

        return Scaffold(
          appBar: APDefaultAppBar(
            title: 'Избранное',
          ),
          body: SafeArea(
            child: Column(
              children: [
                Flexible(
                  child: APAlbumListView(
                    model: APAlbumListModel(selectedIndex: -1, list: state.audios),
                    onSelect: (audio) => controller.add(APAlbumPageSelectItemEvent(audio: audio)),
                  ),
                ),
                APSongPlayBar(
                  controller: controller.playBarController,
                  onPlayTap: (APSongProgressStateType stateType) {
                    controller.add(
                      APAlbumPageOnPlayEvent(
                        stateType: stateType,
                      ),
                    );
                  },
                ),
              ],
            ),
          ),
        );
      }),
    );
  }
}
