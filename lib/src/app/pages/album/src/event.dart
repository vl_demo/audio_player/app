part of '../index.dart';

abstract class AAPAlbumPageEvent {}

final class APAlbumPageInitEvent extends AAPAlbumPageEvent {
  APAlbumPageInitEvent();
}

final class APAlbumPageSelectItemEvent extends AAPAlbumPageEvent {
  final Audio audio;

  APAlbumPageSelectItemEvent({
    required this.audio,
  });
}

final class APAlbumPageOnPlayEvent extends AAPAlbumPageEvent {
  final APSongProgressStateType stateType;

  APAlbumPageOnPlayEvent({
    required this.stateType,
  });
}
