part of '../index.dart';

class APAlbumPageBlocController extends Bloc<AAPAlbumPageEvent, APAlbumPageStateModel> {
  APAlbumPageBlocController({
    required this.args,
  })  : playBarController = APSongPlayBarController.empty(),
        super(
          const APAlbumPageStateModel(audios: []),
        ) {
    on<APAlbumPageInitEvent>(_onInitEvent);
    on<APAlbumPageSelectItemEvent>(_onSelectItemEvent);
    on<APAlbumPageOnPlayEvent>(_onPlay);

    add(APAlbumPageInitEvent());

    assetsAudioPlayer = AssetsAudioPlayer.newPlayer();

    _subscriptions.add(assetsAudioPlayer.playlistAudioFinished.listen((data) {
      print('playlistAudioFinished : $data');
    }));

    _subscriptions.add(assetsAudioPlayer.audioSessionId.listen((sessionId) {
      print('audioSessionId : $sessionId');
    }));

    assetsAudioPlayer.current.forEach(
      (playing) {
        if (playing != null) {
          final audio = _find(state.audios, playing.audio.assetAudioPath);

          playBarController.headerData = APSongHeaderPlayBarModel(
            title: audio.metas.title ?? '',
            subtitle: '${audio.metas.artist} "${audio.metas.album}"',
            stateType: APSongProgressStateType.pause,
            imagePath: audio.metas.image?.path,
          );
        }
      },
    );

    assetsAudioPlayer.isPlaying.forEach((element) {
      playBarController.stateType = element ? APSongProgressStateType.pause : APSongProgressStateType.play;
    });

    assetsAudioPlayer.realtimePlayingInfos.forEach((infos) {
      if (infos.duration.inMilliseconds == 0) {
        return;
      }

      final double progress = infos.currentPosition.inMilliseconds / infos.duration.inMilliseconds;

      playBarController.progressData = APSongProgressPlayBarModel(
        secondsTotal: infos.duration.inSeconds,
        progress: progress,
        stateType: APSongProgressStateType.play,
      );
    });
  }

  final APAlbumPageArgs args;
  final APSongPlayBarController playBarController;

  late AssetsAudioPlayer assetsAudioPlayer;
  final List<StreamSubscription> _subscriptions = [];

  @override
  Future<void> close() async {
    assetsAudioPlayer.dispose();

    super.close();
  }

  void openPlayer() async {
    await assetsAudioPlayer.open(
      Playlist(audios: state.audios, startIndex: 0),
      showNotification: true,
      autoStart: true,
    );
  }

  void _onInitEvent(
    APAlbumPageInitEvent event,
    Emitter<APAlbumPageStateModel> emit,
  ) {
    emit.call(
      state.copyWith(
        audios: [
          Audio(
            Assets.audioKorolISHutDurakIMolniya,
            metas: Metas(
              id: 'Punk',
              title: 'Дурак и молния',
              artist: 'Король и Шут',
              album: 'Камнем по голове',
              image: const MetasImage.asset(
                Assets.imageKorolIShutKamnemPoGolove,
              ),
            ),
          ),
          Audio(
            Assets.audioKorolISHutPrygnuSoSkaly,
            metas: Metas(
              id: 'Punk',
              title: 'Прыгну со скалы',
              artist: 'Король и Шут',
              album: 'Акустический альбом',
              image: const MetasImage.asset(
                Assets.imageKorolIShutAcousticAlbum,
              ),
            ),
          ),
          Audio(
            Assets.audioNautilusPompiliusGorokhovyeZerna,
            metas: Metas(
              id: 'Punk',
              title: 'Гороховые зерны',
              artist: 'Наутилус Помпилиус',
              album: 'Разлука',
              image: const MetasImage.asset(
                Assets.imageRazluka,
              ),
            ),
          ),
          Audio(
            Assets.audioVyacheslavButusovYUPiterSerdceKamnya,
            metas: Metas(
              id: 'Имя рек Сердце камня',
              title: 'Сердце камня',
              artist: 'U-Piter',
              album: 'Имя рек',
              image: const MetasImage.asset(
                Assets.imageImyaRekCover,
              ),
            ),
          ),
          Audio(
            Assets.audioVyacheslavButusovYUPiterStrangliya,
            metas: Metas(
              id: 'Имя рек Странглия',
              title: 'Странглия',
              artist: 'U-Piter',
              album: 'Имя рек',
              image: const MetasImage.asset(
                Assets.imageImyaRekCover,
              ),
            ),
          ),
        ],
      ),
    );
  }

  void _onSelectItemEvent(
    APAlbumPageSelectItemEvent event,
    Emitter<APAlbumPageStateModel> emit,
  ) {
    assetsAudioPlayer.open(
      event.audio,
      showNotification: true,
      autoStart: true,
    );
  }

  Audio _find(List<Audio> source, String fromPath) {
    return source.firstWhere((element) => element.path == fromPath);
  }

  void _onPlay(
    APAlbumPageOnPlayEvent event,
    Emitter<APAlbumPageStateModel> emit,
  ) {
    switch (event.stateType) {
      case APSongProgressStateType.play:
        assetsAudioPlayer.play();
        playBarController.stateType = APSongProgressStateType.pause;
        break;
      case APSongProgressStateType.pause:
        assetsAudioPlayer.pause();
        playBarController.stateType = APSongProgressStateType.play;
        break;
      case APSongProgressStateType.stop:
        assetsAudioPlayer.stop();
        playBarController.stateType = APSongProgressStateType.play;
        break;
    }
  }
}
