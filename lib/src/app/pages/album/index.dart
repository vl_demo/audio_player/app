import 'dart:async';

import 'package:ap_app/generated/assets.dart';
import 'package:ap_app/src/app/ui_components/body/lists/album/index.dart';
import 'package:ap_ui_kit/ap_ui_kit.dart';
import 'package:assets_audio_player/assets_audio_player.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'index.dart';

export 'src/model.dart';

part 'src/controller.dart';
part 'src/event.dart';
part 'src/view.dart';
