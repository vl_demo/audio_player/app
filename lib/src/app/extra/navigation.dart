import 'package:flutter/material.dart';

import '../pages/album/index.dart';

class VLPathRoutes {
  static const album = '/album';
}

abstract class INavigationService {
  final Map<String, Widget Function(BuildContext)> routes = {};

  RouteFactory? get onGenerateRoute;

  String get initRouteName;

  final GlobalKey<NavigatorState> navigatorKey = GlobalKey<NavigatorState>();

  /// Routes
  bool canPop();

  void routePop();

  /// Routes with args
  void routeAlbum(APAlbumPageArgs args);
}

class NavigationService implements INavigationService {
  @override
  final GlobalKey<NavigatorState> navigatorKey = GlobalKey<NavigatorState>();

  @override
  String get initRouteName => VLPathRoutes.album;

  @override
  RouteFactory? get onGenerateRoute => (settings) {
        switch (settings.name) {
          case VLPathRoutes.album:
            return MaterialPageRoute(
              builder: (context) => APAlbumPageView(
                args: settings.arguments == null ? APAlbumPageArgs(title: '') : settings.arguments as APAlbumPageArgs,
              ),
            );
        }
        return null;
      };

  @override
  final routes = <String, Widget Function(BuildContext)>{};

  @override
  void routePop() => Navigator.pop(navigatorKey.currentContext!);

  @override
  bool canPop() {
    return Navigator.canPop(navigatorKey.currentContext!);
  }

  @override
  void routeAlbum(APAlbumPageArgs args) {
    Navigator.pushNamed(
      navigatorKey.currentContext!,
      VLPathRoutes.album,
      arguments: args,
    );
  }
}
