import 'package:ap_app/src/app/extra/navigation.dart';
import 'package:get_it/get_it.dart';

/// Is a smart move to make your Services intiialize before you run the Flutter app.
/// as you can control the execution flow (maybe you need to load some Theme configuration,
/// apiKey, language defined by the User... so load SettingService before running ApiService.
/// so GetMaterialApp() doesnt have to rebuild, and takes the values directly.
///
///

class _LocalKeyPathes {
  static const path = 'assets/credentials/stage.yaml';
}

class _CredentialsDebugConfigKeys {
  static const email = 'email';
  static const password = 'password';
}

class Services {
  static void initServices() {
    //GetIt.I.registerSingletonAsync<IAppRepository>(() async => AppRepository());
  }

  static void initNavigationService() async => GetIt.I.registerSingleton<INavigationService>(
        NavigationService(),
      );
}
