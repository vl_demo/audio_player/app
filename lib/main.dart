import 'dart:async';

import 'package:ap_app/src/app/extra/navigation.dart';
import 'package:ap_ui_kit/ap_ui_kit.dart';
import 'package:bloc/bloc.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:get_it/get_it.dart';

import 'src/data/extra/services/services.dart';

Future<void> main() async {
  Bloc.observer = const GlobalObserver();

  Services.initNavigationService();

  final app = MaterialApp(
    color: Colors.white,
    theme: APThemes.defaultTheme,
    navigatorKey: GetIt.I.get<INavigationService>().navigatorKey,
    title: 'A-Player',
    routes: GetIt.I.get<INavigationService>().routes,
    onGenerateRoute: GetIt.I.get<INavigationService>().onGenerateRoute,
    localizationsDelegates: AppLocalizations.localizationsDelegates,
    supportedLocales: AppLocalizations.supportedLocales,
    initialRoute: GetIt.I.get<INavigationService>().initRouteName,
  );

  runApp(
    _App(
      child: app,
    ),
  );
}

/// {@template counter_observer}
/// [BlocObserver] for the counter application which
/// observes all state changes.
/// {@endtemplate}
class GlobalObserver extends BlocObserver {
  /// {@macro counter_observer}
  const GlobalObserver();

  @override
  void onChange(BlocBase<dynamic> bloc, Change<dynamic> change) {
    super.onChange(bloc, change);

    print('${bloc.runtimeType} $change');
  }
}

class _App extends StatefulWidget {
  final MaterialApp child;

  const _App({required this.child});

  @override
  State<_App> createState() {
    return _AppState();
  }
}

class _AppState extends State<_App> with WidgetsBindingObserver {
  @override
  void initState() {
    Services.initServices();

    super.initState();
  }

  @override
  Future<void> didChangeAppLifecycleState(AppLifecycleState state) async {
    switch (state) {
      case AppLifecycleState.paused:
        break;
      case AppLifecycleState.resumed:
        break;
      default:
        break;
    }

    super.didChangeAppLifecycleState(state);
  }

  @override
  Widget build(BuildContext context) {
    return widget.child;
  }
}
