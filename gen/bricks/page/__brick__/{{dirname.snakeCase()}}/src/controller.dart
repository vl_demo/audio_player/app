part of '../index.dart';

mixin {{classname.pascalCase()}}PageControllerDelegate {
  void onBackDelegate();
  void onSelectItemDelegate({
    required dynamic value,
  });
}

class {{classname.pascalCase()}}PageController extends Bloc<BasePageEvent, {{classname.pascalCase()}}PageModel>
//     implements {{classname.pascalCase()}}ListControllerDelegate
{
{{classname.pascalCase()}}PageController({
    required this.args,
    required this.delegate,
    data,
  }) : super(
    data ??
        const {{classname.pascalCase()}}PageModel(
          loadStatus: PageLoadStatus.none,
        ),
  ) {
    // listController.delegate = this;

    on<{{classname.pascalCase()}}PageBackEvent>(_onBackEvent);
    on<{{classname.pascalCase()}}PageSelectItemEvent>(_onSelectItemEvent);
  }

  final {{classname.pascalCase()}}PageArgs args;
  final {{classname.pascalCase()}}PageBlocDelegate? delegate;
  // final listController = {{classname.pascalCase()}}ListBlocController();

  void _onBackEvent({{classname.pascalCase()}}PageBackEvent event, Emitter<{{classname.pascalCase()}}PageModel> emit) {
    delegate?.onBackDelegate();
  }

  void _onSelectItemEvent(
    {{classname.pascalCase()}}PageSelectItemEvent event,
    Emitter<{{classname.pascalCase()}}PageModel> emit,
  ) {
    delegate?.onSelectItemDelegate(
      value: event.data,
    );
  }

  // TODO: {{classname.pascalCase()}}ListControllerDelegate
  /***
  @override
  void onSelectItem({
    required data,
    required int index,
    }) {
      add(
        {{classname.pascalCase()}}PageSelectItemEvent(
          data: data,
          index: index,
        ),
      );
  }
   */
}
