part of '../index.dart';

final class {{classname.pascalCase()}}PageBackEvent extends BaseEvent {}


final class {{classname.pascalCase()}}PageSelectItemEvent extends BaseEvent {
final dynamic data;
final int index;

{{classname.pascalCase()}}PageSelectItemEvent({
required this.index,
required this.data,
});
}
