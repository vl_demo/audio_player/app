part of '../index.dart';

class {{classname.pascalCase()}}PageArgs {
  {{classname.pascalCase()}}PageArgs({
    required this.value,
  });

  final dynamic value;
}

class {{classname.pascalCase()}}PageView
    extends BasePageView<{{classname.pascalCase()}}PageController, {{classname.pascalCase()}}PageModel, {{classname.pascalCase()}}PageController>
    implements {{classname.pascalCase()}}PageControllerDelegate {
  const {{classname.pascalCase()}}PageView({
required this.args, Key? key,})
      : super(
    key: key,
    backgroundColor: AppColors.dustyPink,
  );

  final {{classname.pascalCase()}}PageArgs args;

  @override
  Widget body(context, state, controller) {
    return const Center(
      child: Text('{{classname.pascalCase()}}PageView'),
    );
  }

  @override
  {{classname.pascalCase()}}PageBlocController createBloc(BuildContext context) {
    return {{classname.pascalCase()}}PageBlocController(args: args, delegate: this,);
  }

  @override
  void listenerBloc(context, state) {
    switch (state.loadStatus) {
      case PageLoadStatus.empty:
        break;
      case PageLoadStatus.none:
        break;
      case PageLoadStatus.loading:
        break;
      case PageLoadStatus.loaded:
        break;
    }
  }

  @override
  ObstructingPreferredSizeWidget? cupertinoAppBar(context, state, controller) {
    // TODO: Need remove method if App Bar is absent

    return null;
  }

  @override
  void onBackDelegate() => GetIt.I.get<INavigationService>().routePop();

@override
  void onSelectItemDelegate({
    required dynamic value,
  }) {
    // TODO: template args navigate to the next screen

    /***
    Navigator.pushNamed(
      NavigationService.navigatorKey!.currentContext!,
        VLPathRoutes.store,
        arguments: StorePageArgs(
        store: store,
      ),
    );*/
  }
}
